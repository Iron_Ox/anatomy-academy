All art used in the game is either hand drawn or copyright-free, the followiing is a list of assets used in the game development :

https://www.gameart2d.com/free-game-gui.html
https://doomcaster.itch.io/button-assets
https://kenney.nl/assets/ui-pack-space-expansion
https://commons.wikimedia.org/wiki/File:Lobes_of_the_brain_UKR.png
https://commons.wikimedia.org/wiki/File:201805_human_skeleton_anim.png